package com.andriy;

import com.andriy.generics.Contaner;
import com.andriy.generics.Men;
import com.andriy.generics.Person;
import com.andriy.generics.Women;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;


public class Application {
    private static Logger logger1= LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        logger1.trace("This is a trace manager");
        logger1.debug("This is a debug manager");
        logger1.info("This is a info manager");
        logger1.warn("This is a warn manager");
        logger1.error("This is a error manager");
        logger1.fatal("This is a fatal manager");
        Contaner contaner = new Contaner();

        List<Men> menList = new ArrayList<>();
        menList.add(new Men("Kolya"));
        menList.add(new Men("Vasya"));

        List<Women> womenList = new ArrayList<>();
        womenList.add(new Women("Olya"));
        womenList.add(new Women("Oksana"));

        List<Person> personList = new ArrayList<>();
        personList.add(new Person("Inna"));
        addToList(personList, new Men("Petro"));
        printList(personList);
        System.out.println("1-------------------");

        contaner.copy(menList, personList);

        printList(personList);
        System.out.println("2--------------------");
        contaner.copy2(personList, womenList);
        printList(personList);
        /**
         * Sorting String using PriorityQueue
         */
        Queue<String> queue = new PriorityQueue<>();
        queue.add("Vasya");
        queue.add("Kolya");
        queue.add("Andriy");
        queue.add("Oksana");
        queue.add("Inna");
        while (!queue.isEmpty()) {
            System.out.println(queue.poll());
        }

/**
 *   Sorting Integer using PriorityQueue
 */
        PriorityQueue<Integer> integers = new PriorityQueue<>();
        Random rand = new Random();
        for (int i = 0; i < 10; i++) {
            integers.add(new Integer(rand.nextInt(20)));
        }
        for (Integer integer : integers) {
            System.out.print(integer + ", ");
        }
        int x = integers.size();
        System.out.println();
        for (int i = 0; i < x; i++) {
            Integer in = integers.poll();
            System.out.print(in + ", ");
        }


    }

    static boolean find(List<? extends Person> all, Person p) {
        return true;
    }

    static <T extends Person> boolean find2(List<T> all, T p) {
        return true;
    }

    static void printList(List<?> list) {
        for (Object o : list) {
            System.out.println(o);
        }
    }

    static void printList2(List<? extends Person> list) {
        for (Person person : list) {
            System.out.println(person);
        }
    }

    static void addToList(List<? super Person> list, Person o) {
        list.add(o);
    }
}
