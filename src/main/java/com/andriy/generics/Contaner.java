package com.andriy.generics;

import java.util.Collection;
import java.util.List;

public class Contaner<T extends Person & Comparable<T>> {

    public <E> void output(Collection<E> collection) {
        for (E e : collection) {
            System.out.println(e);
        }
    }
    public void output(List<Integer> integerList){
        for (Integer integer : integerList) {
            System.out.println(integer);
        }
    }

    /**
     * Combining the two lists
     * @param src
     * @param dest
     */
    public void copy(List<Person> src, List<Person> dest){
        for (Person person : src) {
            dest.add(person);
        }
    }

    /**
     * Write one list to another list
     * @param dst
     * @param src
     * @param <T>
     */
    public <T> void copy2(List<? super T> dst, List<? extends T> src){
        for (int i = 0; i < src.size(); i++) {
             dst.set(i, src.get(i));
        }
    }
}
