package com.andriy.generics;

public class Men extends Person implements Comparable<Men> {
    public Men(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Men{" + super.toString() +'}';
    }

    @Override
    public int compareTo(Men o) {
        return 0;
    }
}
