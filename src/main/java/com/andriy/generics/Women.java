package com.andriy.generics;

public class Women extends Person {
    public Women(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Women{" + super.toString() + '}';
    }
}
