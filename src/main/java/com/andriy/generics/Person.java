package com.andriy.generics;

import java.util.Objects;

public class Person {
    private String name;
    protected final int number;
    private static int count = 0;

    public Person(String name) {
        this.name = name;
        count++;
        number = count;
    }

    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;
        return this;
    }

    public int getCount() {
        return count;
    }

    public Person setCount(int count) {
        this.count = count;
        return this;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", number=" + number +
                '}';
    }
}
