package com.andriy.arrays;

import com.sun.glass.ui.Size;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static java.lang.Integer.SIZE;

public class Application {
    public static void main(String[] args) {
        Random r = new Random();
        /**
         * заповнюєм перший масив випадковими числами
         */
        int[] mas1 = new int[10];
        for (int i = 0; i < mas1.length; i++) {
            mas1[i] = r.nextInt(10);
        }
/**
 * Заповнюєм другий масив випадковими числами
 */
        int[] mas2 = new int[7];
        for (int i = 0; i < mas2.length; i++) {
            mas2[i] = r.nextInt(10);
        }

        System.out.print("mas1 : ");
        System.out.println(Arrays.toString(mas1));
        System.out.print("mas2 : ");
        System.out.println(Arrays.toString(mas2));

        int x = mas1.length + mas2.length;
        int[] mas5 = new int[x];
        System.out.println("Об'єднання двох масивів : ");
        sumTwoMas(mas1, mas2, mas5);
        System.out.println(Arrays.toString(sumTwoMas(mas1, mas2, mas5)));

        System.out.println("Масив з чисел присутніх в обох масивах : ");
        System.out.println(Arrays.toString(elPresentInBothArrays(mas1, mas2)));

        System.out.println("Масив з елементів присутніх в одному з масивів : ");
        System.out.println(Arrays.toString(elPresentInOnlyOneArray(mas1, mas2)));

        System.out.println("Видалити в масиві всі числа, які повторюються більше двох разів.");
        System.out.println(delDuplEl(mas5));
        System.out.println("Знайти в масиві всі серії однакових елементів, які йдуть підряд, і видалити з них всі елементи крім одного.");
        System.out.println(delSimilarEl(mas5));

    }


    static int[] sumTwoMas(int[] mas1, int[] mas2, int[] mas) {
        System.arraycopy(mas1, 0, mas, 0, mas1.length);
        System.arraycopy(mas2, 0, mas, mas1.length, mas2.length);
        return mas;
    }

    static int[] elPresentInBothArrays(int[] mas1, int[] mas2) {
        int count = 0;
        for (int i = 0; i < mas1.length; i++) {
            for (int j = 0; j < mas2.length; j++) {
                if (mas1[i] == mas2[j]) {
                    count++;
                }
            }
        }
        int[] mas3 = new int[count];
        for (int l = 0; l < mas3.length; ) {
            for (int i = 0; i < mas1.length; i++) {
                for (int j = 0; j < mas2.length; j++) {
                    if (mas1[i] == mas2[j]) {
                        mas3[l] = mas1[i];
                        l++;
                    }
                }
            }
        }
        return mas3;
    }

    static int[] elPresentInOnlyOneArray(int[] mas1, int[] mas2) {

        int count1 = 0;
        int count2 = 0;
        count1 = count(mas1, mas2, count1);
        count2 = count(mas2, mas1, count2);

        int count3 = mas1.length - count1;
        int count4 = mas2.length - count2;
        int count = count3 + count4;
        int[] mas = new int[count];
        int[] mas1Fixed = new int[count3];
        int marker = 0;
        for (int l = 0; l < mas1Fixed.length; l++) {
            for (int i = 0; i < mas1.length; i++) {
                for (int j = 0; j < mas2.length; j++) {
                    if (mas1[i] != mas2[j]) {
                        marker++;
                        if (marker == mas2.length) {
                            mas1Fixed[l] = mas1[i];
                            l++;
                            marker = 0;
                        }
                    } else {
                        j = mas2.length;
                        marker = 0;
                    }
                }
            }
        }
        System.out.println("Пофіксаний перший масив");
        System.out.println(Arrays.toString(mas1Fixed));

        int[] mas2Fixed = new int[count4];
        for (int l = 0; l < mas2Fixed.length; l++) {
            for (int i = 0; i < mas2.length; i++) {
                for (int j = 0; j < mas1.length; j++) {
                    if (mas2[i] != mas1[j]) {
                        marker++;
                        if (marker == mas1.length) {
                            mas2Fixed[l] = mas2[i];
                            l++;
                            marker = 0;
                        }
                    } else {
                        j = mas1.length;
                        marker = 0;
                    }
                }
            }
        }
        System.out.println("Пофіксаний другий масив");
        System.out.println(Arrays.toString(mas2Fixed));
        System.arraycopy(mas1Fixed, 0, mas, 0, mas1Fixed.length);
        System.arraycopy(mas2Fixed, 0, mas, mas1Fixed.length, mas2Fixed.length);
        System.out.println("Об'єднані пофіксані масиви");
        return mas;
    }
    /**
     * заповнення проміжного масиву елементами що не повторюються в іншому масиві
     */
//    static int[] clineList(int[] mas1, int[] mas2, int[] masFixed){
//        int marker = 0;
//        for (int l = 0; l < masFixed.length; l++) {
//            for (int i = 0; i < mas1.length; i++) {
//                for (int j = 0; j < mas2.length; j++) {
//                    if (mas1[i] != mas2[j]) {
//                        marker++;
//                        if (marker == mas1.length){
//                            masFixed[l] = mas1[i];
//                            l++;
//                            marker = 0;
//                        }
//                    }else {
//                        j = mas2.length;
//                        marker = 0;
//                    }
//                }
//            }
//        }
//        return masFixed;
//    }

    /**
     * розрахунок кількості співпадінь
     */
    static int count(int[] mas1, int[] mas2, int count) {
        for (int i = 0; i < mas1.length; i++) {
            for (int j = 0; j < mas2.length; j++) {
                if (mas1[i] == mas2[j]) {
                    count++;
                    j = mas2.length;
                }
            }
        }
        return count;
    }

    static List delDuplEl(int[] mas) {
//        int flag;
//        int count = 0;
//        for (int i = 0; i < mas.length; i++) {
//            flag = 0;
//            for (int j = i + 1; j < mas.length; j++) {
//                if (mas[i] == mas[j]) {
//                    flag++;
//                }
//                if (flag >= 2) {
//                    count++;
//                }
//            }
//
//
//        }
//        System.out.println(count);
        int flag = 0;
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < mas.length; i++) {
            flag = 1;
            for (int j = 0; j < mas.length; j++) {
                if (mas[i] == mas[j]) {
                    flag++;
                }
                }
                if (flag<=3){
                    list.add(mas[i]);
            }
        }
        return list;
    }
    static List delSimilarEl(int[] mas){

        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < mas.length-1 ; i++) {
            if (mas[i] != mas[i+1]){
                list.add(mas[i]);
            }
        }
        list.add(mas[mas.length-1]);
        return list;
    }
}
